#include "Products.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

int main()
{
	std::vector<Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrType;
	float priceWithVat = 0;

	for (std::ifstream inputFile("products.txt"); !inputFile.eof();)
	{
		inputFile >> id >> name >> price >> vat >> dateOrType;
		//validate imput...
		products.emplace_back(id, name, price, vat, dateOrType);
	}

	for (int i = 0; i < products.size(); i++)
	{
		if (products[i].getVat() == 19)
		{
			std::cout << products[i];
			priceWithVat = products[i].getPrice() + (19.00 / 100.00) * products[i].getPrice();
			std::cout << "PRICE WITH VAT = " << priceWithVat << std::endl;
			std::cout << std::endl;
		}
	}

	std::cout << "SORT BY PRICE = 0, SORT BY NAME = 1" << std::endl;
	int option;
	std::cin >> option;

	switch (option) {
		case 0:
		{
			std::sort(products.begin(), products.end(), std::less<Product>());

			for (int i = 0; i < products.size(); i++)
			{
				std::cout << products[i];
				std::cout << std::endl;
			}
			break;
		}
		case 1:
		{
			std::sort(products.begin(), products.end(), std::less_equal<Product>());

			for (int i = 0; i < products.size(); i++)
			{
				std::cout << products[i];
				std::cout << std::endl;
			}
			break;	
		}
	}
	system("pause");
	return 0;
}