#pragma once
#include <cstdint>
#include <string>

class Product
{
public:
	Product(uint16_t id, const std::string& name, float price, uint8_t vat, const std::string& dateOrType);

	uint8_t getVat();
	float getPrice();
	bool operator < (const Product& product) const; 
	bool operator <= (const Product& product) const;

	friend std::ostream& operator<<(std::ostream& out, Product& product);

private:
	uint16_t m_id;
	std::string m_name;
	float m_price;
	uint8_t m_vat;                 // TVA in romana
	std::string m_dateOrType;
};